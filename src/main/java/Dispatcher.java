import visualization.AdditiveWave;
import visualization.SineCosineVisualization;
import visualization.SineVisualization;
import visualization.SineWaveVisualization;

public class Dispatcher {

    public static void main(String[] args){
        new Dispatcher();
    }

    private Dispatcher(){
        init();
    }

    private void init(){
        Thread t1 = new Thread(new Runnable () {
            public void run(){
                AdditiveWave.main("visualization.AdditiveWave");
            }
        });
        Thread t2 = new Thread(new Runnable () {
            public void run(){
                SineCosineVisualization.main("visualization.SineCosineVisualization");
            }
        });
        Thread t3 = new Thread(new Runnable () {
            public void run(){
                SineWaveVisualization.main("visualization.SineWaveVisualization");
            }
        });
        t1.start();
        t2.start();
        t3.start();
        SineVisualization.main("visualization.SineVisualization");
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
