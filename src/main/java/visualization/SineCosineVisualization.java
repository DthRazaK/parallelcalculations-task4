package visualization;

import processing.core.PApplet;

public class SineCosineVisualization extends PApplet {
    float x1, x2, y1, y2;
    float angle1, angle2;
    float scalar = 70;

    private boolean flag = true;

    @Override
    public void settings(){
        size(500, 350);
    }

    @Override
    public void setup() {
        noStroke();
        rectMode(CENTER);
    }

    @Override
    public void draw() {
        if (flag) {
            background(0);

            float ang1 = radians(angle1);
            float ang2 = radians(angle2);

            x1 = width / 2 + (scalar * cos(ang1));
            x2 = width / 2 + (scalar * cos(ang2));

            y1 = height / 2 + (scalar * sin(ang1));
            y2 = height / 2 + (scalar * sin(ang2));

            fill(255);
            rect(width * 0.5f, height * 0.5f, 140, 140);

            fill(0, 102, 153);
            ellipse(x1, height * 0.5f - 120, scalar, scalar);
            ellipse(x2, height * 0.5f + 120, scalar, scalar);

            fill(255, 204, 0);
            ellipse(width * 0.5f - 120, y1, scalar, scalar);
            ellipse(width * 0.5f + 120, y2, scalar, scalar);

            angle1 += 2;
            angle2 += 3;
        }
    }

    @Override
    public void mouseClicked(){
        flag = !flag;
    }
}
