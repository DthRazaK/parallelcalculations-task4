package visualization;

import processing.core.PApplet;

public class SineVisualization extends PApplet {

    private float diameter;
    private float angle;
    private boolean flag;

    public SineVisualization() {
        super();
        angle = 0;
        flag = true;
    }

    @Override
    public void settings(){
        size(500, 350);
    }

    @Override
    public void setup(){
        diameter = height - 10;
        noStroke();
        fill(255, 204, 0);
    }

    @Override
    public void draw(){
        if (flag){
            background(0);

            float d1 = 10 + (sin(angle) * diameter/2) + diameter/2;
            float d2 = 10 + (sin(angle + PI/2)* diameter/2) + diameter/2;
            float d3 = 10 + (sin(angle + PI) * diameter/2)  + diameter/2;

            ellipse(0, height/2, d1, d1);
            ellipse(width/2, height/2, d2, d2);
            ellipse(width, height/2, d3, d3);

            angle += 0.02;
        }
    }

    @Override
    public void mouseClicked(){
        flag = !flag;
    }
}
