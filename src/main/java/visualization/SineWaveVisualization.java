package visualization;


import processing.core.PApplet;

public class SineWaveVisualization extends PApplet {

    private int xspacing = 16;   // How far apart should each horizontal location be spaced
    private int w;              // Width of entire wave

    private float theta = 0.0f;  // Start angle at 0
    private float amplitude = 75.0f;  // Height of wave
    private float period = 500.0f;  // How many pixels before the wave repeats
    private float dx;  // Value for incrementing X, a function of period and xspacing
    private float[] yvalues;  // Using an array to store height values for the wave

    private boolean flag = true;

    @Override
    public void settings(){
        size(500, 350);
    }

    @Override
    public void setup() {
        w = width+16;
        dx = (TWO_PI / period) * xspacing;
        yvalues = new float[w/xspacing];
    }

    @Override
    public void draw() {
        if (flag) {
            background(0);
            calcWave();
            renderWave();
        }
    }

    private void calcWave() {
        // Increment theta (try different values for 'angular velocity' here
        theta += 0.02;

        // For every x value, calculate a y value with sine function
        float x = theta;
        for (int i = 0; i < yvalues.length; i++) {
            yvalues[i] = sin(x)*amplitude;
            x+=dx;
        }
    }

    private void renderWave() {
        noStroke();
        fill(255);
        // A simple way to draw the wave with an ellipse at each location
        for (int x = 0; x < yvalues.length; x++) {
            ellipse(x*xspacing, height/2+yvalues[x], 16, 16);
        }
    }

    @Override
    public void mouseClicked(){
        flag = !flag;
    }
}
